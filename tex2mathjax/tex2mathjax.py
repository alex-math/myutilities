"""
Скрипт tex2mathjax.py заменяет знак $ на \\(, при этом не меняя двойного доллара $$
Использование:

python tex2mathjax.py <in_file.tex> <out_file.tex>

"""
import sys


if len(sys.argv) != 3:
    print('Неверное количество аргументов!')
    print('Использование: ')
    print(f'    python {sys.argv[0]}  <входящий_файл> <выходящий_файл>')
    sys.exit(1)

with open(sys.argv[1], 'r', encoding='utf-8') as f:
    working_string = f.read()

final_string = ''
old_pos = -1
pos = working_string.find('$')
open_bracket = True

while pos != -1:
    if working_string[pos + 1] == '$':
        final_string += working_string[old_pos + 1:pos]
        final_string += '$$'
        old_pos = pos + 1
        pos = working_string.find('$$', old_pos + 1)
        final_string += working_string[old_pos + 1:pos] + '$$'
        pos += 1
    else:
        final_string += working_string[old_pos+1:pos]
        if open_bracket:
            final_string += '\\( '
        else:
            final_string += ' \\)'
        open_bracket = not open_bracket
    old_pos = pos
    pos = working_string.find('$', old_pos + 1)

final_string += working_string[old_pos + 1:]

with open(sys.argv[2], 'w') as f:
    f.write(final_string)
